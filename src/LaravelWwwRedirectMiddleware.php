<?php

namespace LaravelWwwRedirect;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * Class LaravelWwwRedirectMiddleware
 *
 * @package LaravelWwwRedirect
 */
class LaravelWwwRedirectMiddleware
{

    /**
     * @param Request $request
     * @param Closure $next
     *
     * @return RedirectResponse|mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $host = $request->header('host');
        if (Str::startsWith($host, 'www.')){
            $redirectHost = str_replace('www.', '', $host);
            $request->headers->set('host', $redirectHost);
            $redirect = new RedirectResponse($request->fullUrl(), 301);
            return $redirect;
        }
        return $next($request);
    }
}