<?php

namespace LaravelWwwRedirect\Tests;

use Faker\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use LaravelWwwRedirect\LaravelWwwRedirectMiddleware;
use Orchestra\Testbench\TestCase;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\HeaderBag;

/**
 * Class LaravelWwwRedirectMiddlewareTest
 *
 * @package LaravelWwwRedirect\Tests
 */
class LaravelWwwRedirectMiddlewareTest extends TestCase
{
    /**
     * @var array
     */
    private $mocks = [];

    public function testWwwHostnameIsRedirected()
    {
        /**
         * Arrange
         */
        $domainNonWww = $this->faker()->domainName;
        $domainHttp = 'http://' . $domainNonWww;
        $domainPlusWww = 'www.' . $domainNonWww;
        $expectedHttpStatus = 301;
        $this->mocks['Request']->shouldReceive('header')->once()->andReturn($domainPlusWww);
        $this->mocks['HeaderBag']->shouldReceive('set')->once()->with('host', $domainNonWww);
        $this->mocks['Request']->shouldReceive('fullUrl')->once()->andReturn($domainHttp);

        /**
         * Act
         */
        $class = new LaravelWwwRedirectMiddleware();
        $return = $class->handle($this->mocks['Request'], $this->mocks['Closure']);

        /**
         * Assert
         */
        Assert::assertInstanceOf(RedirectResponse::class, $return);
        Assert::assertSame($expectedHttpStatus, $return->getStatusCode());
        Assert::assertSame($domainHttp, $return->getTargetUrl());
    }

    public function testNonWwwHostnameIsNotRedirected()
    {
        /**
         * Arrange
         */
        $domainNonWww = $this->faker()->domainName;
        $expectedReturn = $this->mocks['Closure']();
        $this->mocks['Request']->shouldReceive('header')->once()->andReturn($domainNonWww);
        $this->mocks['HeaderBag']->shouldReceive('set')->never();
        $this->mocks['Request']->shouldReceive('fullUrl')->never();

        /**
         * Act
         */
        $class = new LaravelWwwRedirectMiddleware();
        $return = $class->handle($this->mocks['Request'], $this->mocks['Closure']);

        /**
         * Assert
         */
        Assert::assertSame($expectedReturn, $return);
    }


    /**
     * Return a faker object
     *
     * @return \Faker\Generator
     */
    private function faker()
    {
        return Factory::create();
    }

    public function setUp(): void
    {
        $this->mocks['Request'] = \Mockery::mock(Request::class);
        $this->mocks['HeaderBag'] = \Mockery::mock(HeaderBag::class);
        $randomOutput = $this->faker()->sentence;
        $this->mocks['Closure'] = function () use ($randomOutput) {return $randomOutput;};
        $this->mocks['Request']->headers = $this->mocks['HeaderBag'];
    }
}
